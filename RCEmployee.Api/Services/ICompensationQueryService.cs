﻿using RCEmployees.Services.Common.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCEmployee.Api.Services
{
    public interface ICompensationQueryService
    {
        Task<IEnumerable<CompensationView>> BrowseAsync();

    }
}
