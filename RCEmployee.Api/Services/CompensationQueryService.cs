﻿using RCEmployee.Services.Common.Domain.IRepositories;
using RCEmployees.Services.Common.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCEmployee.Api.Services
{
    public class CompensationQueryService : ICompensationQueryService
    {
        private readonly ICompensationViewRepo _compensationViewRepo;

        public CompensationQueryService(ICompensationViewRepo compensationViewRepo)
        {
            _compensationViewRepo = compensationViewRepo;
        }    

        public async Task<IEnumerable<CompensationView>> BrowseAsync()
        {
            var items = await _compensationViewRepo.BrowseAsync();

            return items;
        }

       
    }

}
