﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RawRabbit.Extensions.Client;
using RCEmployee.Api.Services;
using RCEmployee.Services.Common.Domain.IRepositories;
using RCEmployee.Services.Common.Repositories;
using RCEmployees.Common.Events;
using RCEmployees.Common.Mongo;
using RCEmployees.Common.RabbitMq;

namespace RCEmployee.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddRabbitMq();

            services.AddScoped<ICompensationQueryService, CompensationQueryService>();

            //Persistance
            services.AddMongoDB(Configuration);

            services.AddScoped<ICompensationViewRepo, CompensationViewRepo>();
            

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
