﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RawRabbit;
using RCEmployees.Common.Commands;

namespace RCEmployee.Api.Controllers
{

    [Route("[controller]")]
    public class BenefitsController : Controller
    {
        private readonly IBusClient _busClient;

        public BenefitsController(IBusClient busClient)
        {
            _busClient = busClient;
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody]AddBenefit command)
        {
            //Publish the command!
            command.Id = Guid.NewGuid();
            command.CreatedAt = DateTime.UtcNow;
            await _busClient.PublishAsync(command);

            //Return 200
            return Accepted($"benefits/{command.Id}");
        }
    }

}