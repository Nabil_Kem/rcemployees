﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RawRabbit;
using RCEmployees.Common.Commands;

namespace RCEmployee.Api.Controllers
{

    [Route("[controller]")]
    public class VacationDaysController : Controller
    {
        private readonly IBusClient _busClient;

        public VacationDaysController(IBusClient busClient)
        {
            _busClient = busClient;
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody]AddVacationDay command)
        {
            //Publish the command!
            command.Id = Guid.NewGuid();
            command.CreatedAt = DateTime.UtcNow;
            await _busClient.PublishAsync(command);

            //Return 200
            return Accepted($"salaries/{command.Id}");
        }
    }

}