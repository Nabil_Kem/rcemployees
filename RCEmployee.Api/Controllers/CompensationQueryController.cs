﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RawRabbit;
using RCEmployees.Common.Commands;
using RCEmployee.Api.Services;

namespace RCEmployee.Api.Controllers
{
    [Route("[controller]")]
    public class CompensationQueryController : Controller
    {
        private readonly ICompensationQueryService _compensationQueryService;

        public CompensationQueryController(ICompensationQueryService compensationQueryService)
        {
            _compensationQueryService = compensationQueryService;
        }

        // GET: api/values
        [HttpGet("")]
        public async Task<IActionResult> Get()
        {
            var items = await _compensationQueryService.BrowseAsync();

            return Ok(items);

        }
    }

}