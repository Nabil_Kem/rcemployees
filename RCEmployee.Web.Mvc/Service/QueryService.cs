﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RCEmployee.Web.Mvc.Infrastructure;
using RCEmployees.Services.Common.Domain.Model;

namespace RCEmployee.Web.Mvc.Service
{
    public class QueryService : IQueryService
    {
        private readonly IOptionsSnapshot<AppSettings> _settings;
        private readonly IHttpClient _apiClient;


        private readonly string _apiGatewayBaseUrl;

        public QueryService(IOptionsSnapshot<AppSettings> settings, IHttpClient httpClient)
        {
            _settings = settings;
            _apiClient = httpClient;        

            _apiGatewayBaseUrl = $"{_settings.Value.ApiGatwayUrl}";
        }

        public async Task<IEnumerable<CompensationView>> GetAllCompensationsAsync()
        {
            //http://docs.identityserver.io/en/release/quickstarts/1_client_credentials.html

            var response = await _apiClient.GetStringAsync($"{_apiGatewayBaseUrl}/compensationquery/");

            var items = new List<CompensationView>();

            var comps = JArray.Parse(response);            

            foreach (var c in comps.Children<JObject>())
            {
                items.Add(new CompensationView( 
                                                Guid.Parse(c.Value<string>("id")),
                                                Guid.Parse(c.Value<string>("userId")),
                                                c.Value<int>("employeeId"),
                                                c.Value<string>("name"), 
                                                c.Value<decimal>("totalSalary"),
                                                c.Value<string>("benefits"), 
                                                c.Value<int>("days") ));                
            }

            return items;
        }
    }
}
