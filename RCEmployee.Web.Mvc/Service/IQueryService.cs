﻿using RCEmployees.Services.Common.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCEmployee.Web.Mvc.Service
{
    public interface IQueryService
    {
        Task<IEnumerable<CompensationView>> GetAllCompensationsAsync();
    }
}
