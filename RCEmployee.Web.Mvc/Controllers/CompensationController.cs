﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RCEmployee.Web.Mvc.Service;
using Microsoft.AspNetCore.Authorization;

namespace RCEmployee.Web.Mvc.Controllers
{
    public class CompensationController : Controller
    {
        private readonly IQueryService _queryService;

        public CompensationController(IQueryService queryService)
        {
            _queryService = queryService;
        }

        [Authorize]
        public async Task<IActionResult> Index()
        {
            var items = await _queryService.GetAllCompensationsAsync();

            return View(items);
        }
    }
}