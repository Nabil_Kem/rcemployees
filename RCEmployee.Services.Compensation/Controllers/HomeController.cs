﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace RCEmployee.Services.Compensation.Controllers
{
    [Route("")]
    public class HomeController : Controller
    {
        // GET: api/values
        [HttpGet("")]
        public IActionResult Get()
        {
            return Content("Hello from the Compensation Service!");
        }

    }
}