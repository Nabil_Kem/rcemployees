﻿using RawRabbit;
using RCEmployee.Services.Compensation.Services;
using RCEmployees.Common.Commands;
using RCEmployees.Common.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCEmployee.Services.Compensation.Handlers
{
    public class AddSalaryHandler: ICommandHandler<AddSalary>
    {
       
        private readonly IBusClient _busClient;
        private readonly ISalaryService _salaryService;

        public AddSalaryHandler(IBusClient busClient, ISalaryService salaryService)
        {
            _busClient = busClient;
            _salaryService = salaryService;
        }

        public async Task HandleAsync(AddSalary command)
        {
            Console.WriteLine($"Adding Salary : '{command.Id}' for user: '{command.EmployeeID}'.");
            try
            {
                //Persiste the object
                await _salaryService.AddAsync(command.Id, command.UserId, command.EmployeeID, command.Amount, command.WeekPeriod, command.CreatedAt);

                //Publish the event!!
                await _busClient.PublishAsync(new SalaryAdded(command.Id,  command.UserId, command.EmployeeID, command.Amount, command.WeekPeriod, command.CreatedAt));
                
                return;
            }
             catch (Exception ex)
            {                
                Console.WriteLine($"Error while adding salary : '{ex.Message}'");

                await _busClient.PublishAsync(new AddSalaryRejected(command.EmployeeID, ex.Message, "error"));
            }
        }

    }
}
