﻿using MongoDB.Driver;
using RCEmployee.Services.Common.Domain.IRepositories;
using RCEmployee.Services.Common.Domain.Model;
using RCEmployees.Common.Mongo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCEmployee.Services.Compensation.Services
{
    public class CustomMongoSeeder : MongoSeeder
    {
        private readonly IBenefitTypeRepo _benefitTypeRepo;
        private readonly IEmployeeRepo _employeeRepo;

        public CustomMongoSeeder(IMongoDatabase database, IBenefitTypeRepo benefitTypeRepo, IEmployeeRepo employeeRepo) : base(database)
        {
            _benefitTypeRepo = benefitTypeRepo;
            _employeeRepo = employeeRepo;
        }

        protected override async Task CustomSeedAsync()
        {
            var listBenefits = new List<string>
            {
                "Pension",
                "Life Ensurance",
                "Health Care"
            };

            var listEmployees = new List<Employee>
            {
                new Employee(123,"Will","Smith"),
                new Employee(456,"John","Doe"),
                new Employee(789,"Albert","Einstein"),
            };
            //await Task.WhenAll(listBenefits.Select(x => _benefitTypeRepo.AddAsync(new BenefitType(x))));

            var tasks = new List<Task>();
            tasks.AddRange(listBenefits.Select(x => _benefitTypeRepo.AddAsync(new BenefitType(x))));
            tasks.AddRange(listEmployees.Select(emp => _employeeRepo.AddAsync(emp)));

            Task.WhenAll(tasks).Wait();

        }
    }

}
