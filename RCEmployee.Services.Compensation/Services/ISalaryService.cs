﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCEmployee.Services.Compensation.Services
{
    public interface ISalaryService
    {
        Task AddAsync(Guid id, Guid userId, int employeeId, decimal amount, byte weekPeriod, DateTime createdAt);
    }
}
