﻿using RCEmployee.Services.Common.Domain.IRepositories;
using RCEmployee.Services.Common.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCEmployee.Services.Compensation.Services
{
    public class SalaryService : ISalaryService
    {
        private readonly ISalaryRepo _SalaryRepo;
        private readonly IEmployeeRepo _employeeRepo;

        public SalaryService(ISalaryRepo SalaryRepo, IEmployeeRepo employeeRepo)
        {
            _SalaryRepo = SalaryRepo;
            _employeeRepo = employeeRepo;
        }

        public async Task AddAsync(Guid id,  Guid userId, int employeeId, decimal amount, byte weekPeriod, DateTime createdAt)
        {
            try
            {
                var emp = await _employeeRepo.GetAsync(employeeId);
                if (emp == null)
                {
                    throw new Exception($"Employee : '{employeeId}' was not found.");
                }
                var salary = new Salary(id, emp, userId, amount, weekPeriod, createdAt);
                await _SalaryRepo.AddAsync(salary);
            }
            catch (Exception e)
            {

                throw;
            }
            
        }
    }

}
