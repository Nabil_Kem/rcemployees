﻿using RCEmployees.Common.Commands;
using RCEmployees.Common.Events;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RawRabbit;
using RawRabbit.vNext;


namespace RCEmployees.Common.RabbitMq
{
    public static class Extensions
    {
        //public static void WithCommandHandlerAsync<TCommand>(this IBusClient bus, ICommandHandler<TCommand> handler) where TCommand : ICommand
        //{
        //    bus.SubscribeAsync<TCommand>(async (msg, context) => await handler.HandleAsync(msg),
        //        cfg => cfg.WithQueue(q => q.WithName(GetQueueName<TCommand>())));
        //}

        //public static void WithEventHandlerAsync<TEvent>(this IBusClient bus, IEventHandler<TEvent> handler) where TEvent : IEvent
        //{
        //    bus.SubscribeAsync<TEvent>(async (msg, context) => await handler.HandleAsync(msg),
        //     cfg => cfg.WithQueue(q => q.WithName(GetQueueName<TEvent>())));
        //}

        //private static string GetQueueName<T>()
        //{
        //    return $"{Assembly.GetEntryAssembly().GetName()}/{typeof(T).Name}";
        //}

        public static void AddRabbitMq(this IServiceCollection services)
        {
            services.AddRawRabbit(cfg => cfg.AddJsonFile("rawrabbit.json"));
        }
    }

}
