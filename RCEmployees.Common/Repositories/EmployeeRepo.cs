﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using RCEmployee.Services.Common.Domain.IRepositories;
using RCEmployee.Services.Common.Domain.Model;

namespace RCEmployee.Services.Common.Repositories
{
    public class EmployeeRepo : IEmployeeRepo
    {
        private readonly IMongoDatabase _database;

        public EmployeeRepo(IMongoDatabase database)
        {
            _database = database;
        }

        private IMongoCollection<Employee> Collection
        {
            get { return _database.GetCollection<Employee>("Employees"); }
        }

        public async Task AddAsync(Employee Employee)
        {
            await Collection.InsertOneAsync(Employee);
        }

        public async Task<IEnumerable<Employee>> BrowseAsync()
        {
            return await Collection.AsQueryable().ToListAsync();
        }

        public async Task DeleteAsync(int EmployeeId)
        {
            await Collection.DeleteOneAsync(x => x.Id == EmployeeId);
        }

        public async Task<Employee> GetAsync(int EmployeeId)
        {
            return await Collection.AsQueryable().Where(x => x.Id == EmployeeId).FirstOrDefaultAsync();
           
        }
    }
}
