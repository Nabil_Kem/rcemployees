﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using RCEmployee.Services.Common.Domain.IRepositories;
using RCEmployee.Services.Common.Domain.Model;

namespace RCEmployee.Services.Common.Repositories
{
    public class BenefitRepo : IBenefitRepo
    {
        private readonly IMongoDatabase _database;

        public BenefitRepo(IMongoDatabase database)
        {
            _database = database;
        }

        private IMongoCollection<Benefit> Collection
        {
            get { return _database.GetCollection<Benefit>("Benefits"); }
        }

        public async Task AddAsync(Benefit Benefit)
        {
            await Collection.InsertOneAsync(Benefit);
        }

        public async Task<IEnumerable<Benefit>> BrowseAsync()
        {
            return await Collection.AsQueryable().ToListAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            await Collection.DeleteOneAsync(x => x.Id == id);
        }

        public async Task<Benefit> GetAsync(Guid id)
        {
            return await Collection.AsQueryable().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<string>> GetEmployeeBenefits(int employeeId)
        {
            return await Collection.AsQueryable().Where(x => x.EmployeeID == employeeId).Select(f => f.BenefitType).ToListAsync();

        }

     }
}
