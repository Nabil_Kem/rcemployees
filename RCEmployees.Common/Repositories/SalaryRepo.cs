﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using RCEmployee.Services.Common.Domain.IRepositories;
using RCEmployee.Services.Common.Domain.Model;

namespace RCEmployee.Services.Common.Repositories
{
    public class SalaryRepo : ISalaryRepo
    {
        private readonly IMongoDatabase _database;

        public SalaryRepo(IMongoDatabase database)
        {
            _database = database;
        }

        private IMongoCollection<Salary> Collection
        {
            get { return _database.GetCollection<Salary>("Salaries"); }
        }

        public async Task AddAsync(Salary salary)
        {
            await Collection.InsertOneAsync(salary);
        }

        public async Task<IEnumerable<Salary>> BrowseAsync()
        {
            return await Collection.AsQueryable().ToListAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            await Collection.DeleteOneAsync(x => x.Id == id);
        }

        public async Task<Salary> GetAsync(Guid id)
        {
            return await Collection.AsQueryable().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<decimal> GetTotalSalaryByEmployee(int employeeId)
        {
            var data = await (from row in Collection.AsQueryable()
                              where row.EmployeeID == employeeId
                              select row.Amount).ToListAsync();

           return data.Sum();

          // return await Collection.AsQueryable().Where(x => x.EmployeeID == employeeId).Select(f => f.Amount).SumAsync();
        }
    }
}
