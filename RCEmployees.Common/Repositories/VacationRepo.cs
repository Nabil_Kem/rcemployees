﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using RCEmployee.Services.Common.Domain.IRepositories;
using RCEmployee.Services.Common.Domain.Model;

namespace RCEmployee.Services.Common.Repositories
{
    public class VacationRepo : IVacationRepo
    {
        private readonly IMongoDatabase _database;

        public VacationRepo(IMongoDatabase database)
        {
            _database = database;
        }

        private IMongoCollection<Vacation> Collection
        {
            get { return _database.GetCollection<Vacation>("Vacations"); }
        }

        public async Task AddAsync(Vacation Vacation)
        {
            await Collection.InsertOneAsync(Vacation);
        }

        public async Task<IEnumerable<Vacation>> BrowseAsync()
        {
            return await Collection.AsQueryable().ToListAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            await Collection.DeleteOneAsync(x => x.Id == id);
        }

        public async Task<Vacation> GetAsync(Guid id)
        {
            return await Collection.AsQueryable().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<decimal> GetRemainingByEmployee(int employeeId)
        {
            return await Collection.AsQueryable().Where(x => x.EmployeeID == employeeId).Select(f => f.Days).SumAsync();
        }
    }
}
