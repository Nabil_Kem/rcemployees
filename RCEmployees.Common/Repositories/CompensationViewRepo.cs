﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using RCEmployee.Services.Common.Domain.IRepositories;
using RCEmployee.Services.Common.Domain.Model;
using RCEmployees.Services.Common.Domain.Model;

namespace RCEmployee.Services.Common.Repositories
{
    public class CompensationViewRepo : ICompensationViewRepo
    {
        private readonly IMongoDatabase _database;

        public CompensationViewRepo(IMongoDatabase database)
        {
            _database = database;
        }

        private IMongoCollection<CompensationView> Collection
        {
            get { return _database.GetCollection<CompensationView>("CompensationViews"); }
        }

        public async Task AddAsync(CompensationView entity)
        {
            await Collection.InsertOneAsync(entity);
        }

        public async Task<IEnumerable<CompensationView>> BrowseAsync()
        {
            return await Collection.AsQueryable().ToListAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            await Collection.DeleteOneAsync(x => x.Id == id);
        }

        public async Task<CompensationView> GetByEmpIdAsync(int EmployeeId)
        {
            return await Collection.AsQueryable().FirstOrDefaultAsync(x => x.EmployeeId == EmployeeId);
        }

        public async Task<CompensationView> GetAsync(Guid id)
        {
            return await Collection.AsQueryable().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task UpdateAsync(CompensationView comp)
        {
            var filter = Builders<CompensationView>.Filter.Eq(s => s.Id, comp.Id);
            await Collection.ReplaceOneAsync(filter, comp);
        }
    }
}
