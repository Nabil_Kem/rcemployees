﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using RCEmployee.Services.Common.Domain.IRepositories;
using RCEmployee.Services.Common.Domain.Model;

namespace RCEmployee.Services.Common.Repositories
{
    public class BenefitTypeRepo : IBenefitTypeRepo
    {
        private readonly IMongoDatabase _database;

        public BenefitTypeRepo(IMongoDatabase database)
        {
            _database = database;
        }

        private IMongoCollection<BenefitType> Collection
        {
            get { return _database.GetCollection<BenefitType>("BenefitTypes"); }
        }

        public async Task AddAsync(BenefitType benefitType)
        {
            await Collection.InsertOneAsync(benefitType);
        }

        public async Task<IEnumerable<BenefitType>> BrowseAsync()
        {
            return await Collection.AsQueryable().ToListAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            await Collection.DeleteOneAsync(x => x.Id == id);
        }

        public async Task<BenefitType> GetAsync(Guid id)
        {
            return await Collection.AsQueryable().FirstOrDefaultAsync(x => x.Id == id);
        }

       
    }
}
