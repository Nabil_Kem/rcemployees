﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RCEmployees.Common.Services
{
    public interface IServiceHost
    {
        void Run();
    }

}
