﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using RawRabbit;
using RCEmployees.Common.Commands;
using RCEmployees.Common.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace RCEmployees.Common.Services
{
    public class ServiceHost : IServiceHost
    {
        private readonly IWebHost _webHost;

        public ServiceHost(IWebHost webHost)
        {
            _webHost = webHost;
        }

        public void Run() {
             _webHost.Run();
        }

        public static HostBuilder Create<TStartup>(string[] args) where TStartup : class
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();

            var webHostBuilder = WebHost.CreateDefaultBuilder(args)
                .UseConfiguration(config)
                .UseStartup<TStartup>()
                .UseDefaultServiceProvider(options => options.ValidateScopes = false);

            return new HostBuilder(webHostBuilder.Build());
        }

        //================================================================================================
        public abstract class BuilderBase
        {
            public abstract ServiceHost Build();
        }
        //====================================
        public class HostBuilder : BuilderBase
        {
            private readonly IWebHost _webHost;
            private IBusClient _bus;

            public HostBuilder(IWebHost webHost)
            {
                _webHost = webHost;
            }

            public override ServiceHost Build()
            {
                return new ServiceHost(_webHost);
            }

            public BusBuilder UseRabbitMq()
            {
                _bus = (IBusClient)_webHost.Services.GetService(typeof(IBusClient));

                return new BusBuilder(_webHost, _bus);
            }
        }

        //====================================
        public class BusBuilder : BuilderBase
        {
            private readonly IWebHost _webHost;
            private IBusClient _bus;

            public BusBuilder(IWebHost webHost, IBusClient bus)
            {
                _webHost = webHost;
                _bus = bus;
            }

            public override ServiceHost Build()
            {
                return new ServiceHost(_webHost);
            }

            public BusBuilder SubscribeToCommand<TCommand>() where TCommand : ICommand
            {
                try
                {
                    var handler = (ICommandHandler<TCommand>)_webHost.Services.GetService(typeof(ICommandHandler<TCommand>));

                    //_bus.WithCommandHandlerAsync(handler);
                    _bus.SubscribeAsync<TCommand>(async (msg, context) => await handler.HandleAsync(msg),
                                                                        cfg => cfg.WithQueue(q => q.WithName(GetQueueName<TCommand>())));
                    return this;
                }
                catch (Exception e)
                {

                    throw;
                }
            }

            public BusBuilder SubscribeToEvent<TEvent>() where TEvent : IEvent
            {
                var handler = (IEventHandler<TEvent>)_webHost.Services.GetService(typeof(IEventHandler<TEvent>));
                
                // _bus.WithEventHandlerAsync(handler);
                _bus.SubscribeAsync<TEvent>(async (msg, context) => await handler.HandleAsync(msg),
                 cfg => cfg.WithQueue(q => q.WithName(GetQueueName<TEvent>())));

                return this;
            }

            private static string GetQueueName<T>()
            {
                return $"{Assembly.GetEntryAssembly().GetName()}/{typeof(T).Name}";
            }


        }
    }

}
