﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RCEmployees.Common.Commands
{
    public class AddSalary : IAuthenticatedCommand    {
        
        public Guid Id { get; set; }           //unique id of the command
        public Guid UserId { get; set; }       //User Id of the admin who created the salary

        public int EmployeeID { get; set; } // Employee Id getting the salary
        public decimal Amount { get; set; }
        public byte WeekPeriod { get; set; }  

        public DateTime CreatedAt { get; set; }


    }
}
