﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RCEmployees.Common.Commands
{
    public class AddVacationDay : IAuthenticatedCommand
    {
        public Guid Id { get; set; }           //unique id of the command
        public Guid UserId { get; set; }       //User Id of the admin who add the vacation day

        public int EmployeeID { get; set; } // Employee Id getting the salary
        public Int16 Days { get; set; }
        
        public DateTime CreatedAt { get; set; }
    }
}
