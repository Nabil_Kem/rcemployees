﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RCEmployees.Common.Commands
{
    public class AddBenefit : IAuthenticatedCommand    {
        
        public Guid Id { get; set; }           //unique id of the command
        public Guid UserId { get; set; }       //User Id of the admin who created the salary

        public int EmployeeID { get; set; } // Employee Id getting the benefit
        public string BenefitType { get; set; }

        public DateTime CreatedAt { get; set; }


    }
}
