﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RCEmployees.Common.Events 
{
    public class BenefitAdded : IAuthenticatedEvent
    {
        public Guid Id { get; set; }           //unique id of the command
        public Guid UserId { get; set; }       //User Id of the admin who created the salary
              
        public int EmployeeID { get; set; } // Employee Id getting the benefit
        public string BenefitType { get; set; }

        public DateTime CreatedAt { get; set; }
        

        protected BenefitAdded(Guid id)
        {

        }
        public BenefitAdded(Guid id, Guid userId, int employeeID, string benefitType, DateTime createdAt)
        {
            this.Id = id;
            this.UserId = userId;

            this.EmployeeID = employeeID;
            this.BenefitType = benefitType;
 
            this.CreatedAt = createdAt;

        }

    }
}
