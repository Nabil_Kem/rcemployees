﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RCEmployees.Common.Events
{
    public class CompensationViewRejected : IRejectedEvent
    {
        public string Reason { get; }

        public string Code { get; }

        public int EmployeeID { get; }

        protected CompensationViewRejected()
        {

        }

        public CompensationViewRejected(int employeeID, string reason, string code)
        {
            EmployeeID = employeeID;
            Reason = reason;
            Code = code;
        }
    }
}

