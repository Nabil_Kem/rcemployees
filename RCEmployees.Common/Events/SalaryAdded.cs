﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RCEmployees.Common.Events 
{
    public class SalaryAdded : IAuthenticatedEvent
    {
        public Guid Id { get; set; }           //unique id of the command
        public Guid UserId { get; set; }       //User Id of the admin who created the salary
              
        public int EmployeeID { get; set; } // Employee Id getting the salary
        public decimal Amount { get; set; }
        public byte WeekPeriod { get; set; }

        public DateTime CreatedAt { get; set; }
        

        protected SalaryAdded(Guid id)
        {

        }
        public SalaryAdded(Guid id, Guid userId, int employeeID, decimal amount, byte weekPeriod, DateTime createdAt)
        {
            this.Id = id;
            this.UserId = userId;

            this.EmployeeID = employeeID;
            this.Amount = amount;
            this.WeekPeriod = weekPeriod;

            this.CreatedAt = createdAt;

        }

    }
}
