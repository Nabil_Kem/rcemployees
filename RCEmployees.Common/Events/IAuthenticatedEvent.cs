﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RCEmployees.Common.Events
{
    public interface IAuthenticatedEvent : IEvent
    {
        Guid UserId { get; set; }
    }

}
