﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RCEmployees.Common.Events 
{
    public class VacationDayAdded : IAuthenticatedEvent
    {
        public Guid Id { get; set; }           //unique id of the command
        public Guid UserId { get; set; }       //User Id of the admin who created the salary
              
        public int EmployeeID { get; set; } // Employee Id getting the salary
        public Int16 Days { get; set; }

        public DateTime CreatedAt { get; set; }
        

        protected VacationDayAdded(Guid id)
        {

        }
        public VacationDayAdded(Guid id, Guid userId, int employeeID, Int16 days, DateTime createdAt)
        {
            this.Id = id;
            this.UserId = userId;

            this.EmployeeID = employeeID;
            this.Days = days;

            this.CreatedAt = createdAt;

        }

    }
}
