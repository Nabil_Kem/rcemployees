﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RCEmployees.Common.Events
{
    public class AuthenticateUserRejected : IRejectedEvent
    {
        public string Reason { get; }

        public string Code { get; }

        public Guid Id { get; }

        protected AuthenticateUserRejected()
        {

        }

        public AuthenticateUserRejected(Guid id, string reason, string code)
        {
            Id = id;
            Reason = reason;
            Code = code;
        }
    }
}
