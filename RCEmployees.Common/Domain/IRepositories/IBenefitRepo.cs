﻿using RCEmployee.Services.Common.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCEmployee.Services.Common.Domain.IRepositories
{
    public interface IBenefitRepo
    {
        Task<Benefit> GetAsync(Guid id);
        Task<IEnumerable<Benefit>> BrowseAsync();

        Task AddAsync(Benefit Benefit);
        Task DeleteAsync(Guid id);
        Task<List<string>> GetEmployeeBenefits(int employeeId);
    }
}
