﻿using RCEmployees.Services.Common.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCEmployee.Services.Common.Domain.IRepositories
{
    public interface ICompensationViewRepo
    {
        Task<CompensationView> GetAsync(Guid id);
        Task<CompensationView> GetByEmpIdAsync(int EmployeeId);
        Task<IEnumerable<CompensationView>> BrowseAsync();
        Task AddAsync(CompensationView entity);
        Task DeleteAsync(Guid id);
        Task UpdateAsync(CompensationView comp);
    }
}
