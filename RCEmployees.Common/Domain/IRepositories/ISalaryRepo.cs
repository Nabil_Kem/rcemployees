﻿
using RCEmployee.Services.Common.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCEmployee.Services.Common.Domain.IRepositories
{
    public interface ISalaryRepo
    {
        Task<Salary> GetAsync(Guid id);
        Task<IEnumerable<Salary>> BrowseAsync();        

        Task AddAsync(Salary salary);
        Task DeleteAsync(Guid id);
        Task<decimal> GetTotalSalaryByEmployee(int employeeId);
    }
}
