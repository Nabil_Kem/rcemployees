﻿using RCEmployee.Services.Common.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCEmployee.Services.Common.Domain.IRepositories
{
    public interface IEmployeeRepo
    {
        Task<Employee> GetAsync(int EmployeeId);
        Task<IEnumerable<Employee>> BrowseAsync();

        Task AddAsync(Employee Employee);
        Task DeleteAsync(int EmployeeId);
    }
}
