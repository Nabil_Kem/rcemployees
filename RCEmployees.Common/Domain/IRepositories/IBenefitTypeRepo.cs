﻿using RCEmployee.Services.Common.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCEmployee.Services.Common.Domain.IRepositories
{
    public interface IBenefitTypeRepo
    {
        Task<BenefitType> GetAsync(Guid id);
        Task<IEnumerable<BenefitType>> BrowseAsync();

        Task AddAsync(BenefitType benefitType);
        Task DeleteAsync(Guid id);

    }
}
