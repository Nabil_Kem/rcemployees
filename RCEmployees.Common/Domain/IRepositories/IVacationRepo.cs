﻿
using RCEmployee.Services.Common.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCEmployee.Services.Common.Domain.IRepositories
{
    public interface IVacationRepo
    {
        Task<Vacation> GetAsync(Guid id);
        Task<IEnumerable<Vacation>> BrowseAsync();

        Task AddAsync(Vacation Vacation);
        Task DeleteAsync(Guid id);
        Task<decimal> GetRemainingByEmployee(int employeeId);
    }
}
