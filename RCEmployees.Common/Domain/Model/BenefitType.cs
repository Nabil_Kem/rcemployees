﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCEmployee.Services.Common.Domain.Model
{
    public class BenefitType
    {
        public Guid Id { get; protected set; }
        public string Name { get; set; }

        protected BenefitType()
        {

        }

        public BenefitType(string name)
        {
            Id = Guid.NewGuid();
            Name = name;
        }

    }
}
