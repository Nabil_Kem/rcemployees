﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCEmployee.Services.Common.Domain.Model
{
    public class Salary
    {
        public Guid Id { get; set; }
        [BsonElement("userId")]
        public Guid UserId { get; set; }
        [BsonElement("employeeID")]
        public int EmployeeID { get; set; }
        [BsonElement("createdAt")]
        public DateTime CreatedAt { get; set; }
        [BsonElement("amount")]
        public decimal Amount { get; set; }
        [BsonElement("weekPeriod")]
        public byte WeekPeriod { get; set; }

        protected Salary()
        {
        }

        public Salary(Guid id, Employee employee, Guid userId, decimal amount , byte weekPeriod , DateTime createdAt)
        {
            Id = id;
            EmployeeID = employee == null ? 0 : employee.Id;
            UserId = userId;
            CreatedAt = createdAt;
            Amount = amount;
            WeekPeriod = weekPeriod;
        }

    }
}
