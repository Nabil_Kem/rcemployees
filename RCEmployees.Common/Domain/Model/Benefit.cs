﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCEmployee.Services.Common.Domain.Model
{
    public class Benefit
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public int EmployeeID { get; set; }
        public DateTime CreatedAt { get; set; }

        public string BenefitType { get; set; }

        protected Benefit()
        {
        }

        public Benefit(Guid id, Employee employee, BenefitType benefitType, Guid userId, DateTime createdAt)
        {
            Id = id;
            EmployeeID = employee.Id;
            this.BenefitType = benefitType.Name;
            UserId = userId;
            CreatedAt = createdAt;
        }
    }
}
