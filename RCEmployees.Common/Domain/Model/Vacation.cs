﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCEmployee.Services.Common.Domain.Model
{
    public class Vacation
    {
        public Guid Id { get; set; }          
        public Guid UserId { get; set; }     
        public int EmployeeID { get; set; } 
        public DateTime CreatedAt { get; set; }

        public decimal Days { get; set; }        

        protected Vacation()
        {
        }

        public Vacation(Guid id, Employee employee, Guid userId, decimal days , DateTime createdAt)
        {
            Id = id;
            EmployeeID = employee.Id;
            UserId = userId;
            CreatedAt = createdAt;
            Days = days;
        }

    }
}
