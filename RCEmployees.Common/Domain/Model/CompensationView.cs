﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCEmployees.Services.Common.Domain.Model
{
    public class CompensationView
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public int EmployeeId { get; set; }
        public string Name { get; set; }       
        public decimal TotalSalary { get; set; }
        public string Benefits { get; set; }
        public decimal Days { get; set; }

        protected CompensationView()
        {

        }

        public CompensationView(Guid id, Guid userId, int employeeId, string name, decimal totalSalary, string benefits, decimal days)
        {
            Id = id;
            UserId = userId;
            EmployeeId = employeeId;
            Name = name;           
            TotalSalary = totalSalary;
            Benefits = benefits;
            Days = days;
        }
    }
}
