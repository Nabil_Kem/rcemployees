﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCEmployee.Services.Common.Domain.Model
{
    public class Employee
    {

        public int Id { get;  set; }
        [BsonElement("firstName")]
        public string FirstName { get; set; }
        [BsonElement("lastName")]
        public string LastName { get; set; }

        protected Employee()
        {

        }

        public Employee(int employeeId, string firstName, string lastName)
        {
            Id = employeeId;
            FirstName = firstName;
            LastName = lastName;
        }

    }
}
