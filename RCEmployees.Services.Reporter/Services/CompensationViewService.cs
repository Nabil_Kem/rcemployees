﻿using RCEmployee.Services.Common.Domain.IRepositories;
using RCEmployees.Services.Common.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCEmployee.Services.Reporter.Services
{
    public class CompensationViewService : ICompensationViewService
    {
        private readonly ICompensationViewRepo _compensationViewRepo;
        private readonly IEmployeeRepo _employeeRepo;
        private readonly ISalaryRepo _salaryRepo;
        private readonly IBenefitRepo _benefitRepo;
        private readonly IVacationRepo _vacationRepo;

        public CompensationViewService(ICompensationViewRepo compensationViewRepo, IEmployeeRepo employeeRepo, ISalaryRepo salaryRepo, IBenefitRepo benefitRepo, IVacationRepo vacationRepo)
        {
            _compensationViewRepo = compensationViewRepo;
            _employeeRepo = employeeRepo;
            _salaryRepo = salaryRepo;
            _benefitRepo = benefitRepo;
            _vacationRepo = vacationRepo;
        }

        public async Task UpsertCompensationAsync(Guid id, Guid userId, int employeeId, DateTime createdAt)
        {
            var comp = await _compensationViewRepo.GetByEmpIdAsync(employeeId);

            try
            {
                var emp = await _employeeRepo.GetAsync(employeeId);
                var name = emp.FirstName + ", " + emp.LastName;

                decimal tot = await _salaryRepo.GetTotalSalaryByEmployee(employeeId);
                var totalSalary = tot;

                var ben = await _benefitRepo.GetEmployeeBenefits(employeeId);
                var benefits = string.Join(",", ben.ToArray());

                var remain_days = await _vacationRepo.GetRemainingByEmployee(employeeId);
                var days = remain_days;

                if (comp == null)
                {
                    var newGuid = Guid.NewGuid();

                    var entity = new CompensationView(newGuid, userId, employeeId, name, totalSalary, benefits, days);
                    await _compensationViewRepo.AddAsync(entity);
                }
                else
                {
                    comp.TotalSalary = tot;
                    comp.Benefits = benefits;
                    comp.Days = days;

                    await _compensationViewRepo.UpdateAsync(comp);
                }
            }catch  (Exception e)
            {

            }

        }
      
    }

}
