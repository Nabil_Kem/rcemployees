﻿using RCEmployees.Services.Common.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCEmployee.Services.Reporter.Services
{
    public interface ICompensationViewService
    {
        Task UpsertCompensationAsync(Guid id, Guid userId, int employeeId, DateTime createdAt);
    }
}
