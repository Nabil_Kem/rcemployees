﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RCEmployee.Services.Common.Domain.IRepositories;
using RCEmployee.Services.Common.Repositories;
using RCEmployee.Services.Reporter.Services;
using RCEmployees.Common.Commands;
using RCEmployees.Common.Events;
using RCEmployees.Common.Mongo;
using RCEmployees.Common.RabbitMq;
using RCEmployee.Services.Reporter.Handlers;

namespace RCEmployees.Services.Reporter
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            //Bus
            services.AddRabbitMq();
            services.AddScoped<IEventHandler<SalaryAdded>, SalaryAddedHandler>();

            services.AddScoped<ICompensationViewService, CompensationViewService>();

            //Persistance
            services.AddMongoDB(Configuration);

            services.AddScoped<IBenefitRepo, BenefitRepo>();
            services.AddScoped<ICompensationViewRepo, CompensationViewRepo>();
            services.AddScoped<IEmployeeRepo, EmployeeRepo>();
            services.AddScoped<ISalaryRepo, SalaryRepo>();
            services.AddScoped<IVacationRepo, VacationRepo>();
           
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
