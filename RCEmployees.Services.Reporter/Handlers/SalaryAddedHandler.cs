﻿using RawRabbit;
using RCEmployee.Services.Reporter.Services;
using RCEmployees.Common.Commands;
using RCEmployees.Common.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCEmployee.Services.Reporter.Handlers
{
    public class SalaryAddedHandler : IEventHandler<SalaryAdded>
    {
       
        private readonly IBusClient _busClient;
        private readonly ICompensationViewService _compensationViewService;

        public SalaryAddedHandler(IBusClient busClient, ICompensationViewService compensationViewService)
        {
            _busClient = busClient;
            _compensationViewService = compensationViewService;
        }

        public async Task HandleAsync(SalaryAdded @event)
        {
            try
            {
                //Persiste the object
                await _compensationViewService.UpsertCompensationAsync(@event.Id, @event.UserId, @event.EmployeeID, @event.CreatedAt);
                
                return;
            }
             catch (Exception ex)
            {                
                Console.WriteLine($"Error while adding salary : '{ex.Message}'");

                await _busClient.PublishAsync(new CompensationViewRejected(@event.EmployeeID, ex.Message, "error"));
            }
        }

        
    }
}
