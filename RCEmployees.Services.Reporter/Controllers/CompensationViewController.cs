﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RCEmployee.Services.Reporter.Services;

namespace RCEmployees.Services.Reporter.Controllers
{
    [Route("[controller]")]
    public class CompensationViewController : Controller
    {
        private readonly ICompensationViewService _compensationViewService;

        public CompensationViewController(ICompensationViewService compensationViewService)
        {       
            _compensationViewService = compensationViewService;
        }

        // GET: api/values
        [HttpGet("")]
        public async Task<IActionResult> Get()
        {
            var items = await _compensationViewService.BrowseAsync();

            return Ok(items);
          
        }

    }
}